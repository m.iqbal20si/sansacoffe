<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barista;

class BaristaController extends Controller
{
    public function index()
    {
        $barista = Barista::all();
        return response()->json($barista);
    }

    public function show($id)
    {
        $barista = Barista::find($id);
        return response()->json($barista);
    }

    public function store(Request $request)
    {
        $barista = new Barista;
        $barista->title = $request->title;
        $barista->content = $request->content;
        $barista->save();

        return response()->json($barista);
    }

    public function update(Request $request, $id)
    {
        $barista = Barista::find($id);
        $barista->title = $request->title;
        $barista->content = $request->content;
        $barista->save();

        return response()->json($barista);
    }

    public function destroy($id)
    {
        $barista = Barista::find($id);
        $barista->delete();

        return response()->json(['message' => 'barista berhasil dihapus']);
    }
}
